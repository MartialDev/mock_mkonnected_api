<?php

namespace App\Controller;

use App\Contracts\TokenGeneratorInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Abstract Class AbstractDefaultController
 * @package App\Controller
 * @author     Roland Martial KUATE FOKOM <rolandmartialkuate@gmail.com>
 */
abstract class AbstractDefaultController extends AbstractController
{
    /**
     * @var EntityManager entity manager object.
     */
    protected $em;

    /**
     * @var Request $request current request.
     */
    protected $request;

    /**
     * @var int $nbItemsPerPage Define number of items to show in one page
     */
    protected $nbItemsPerPage = 10;

    /**
     * AbstractDefaultController constructor.
     * @param RequestStack $request
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RequestStack $request, EntityManagerInterface $entityManager)
    {
        $this->request = $request->getCurrentRequest();
        $this->em = $entityManager;
    }
}

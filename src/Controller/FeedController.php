<?php

namespace App\Controller;

use App\Entity\Feed;
use Doctrine\ORM\EntityManager;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class FeedController
 * @package App\Controller
 * @author     Roland Martial KUATE FOKOM <rolandmartialkuate@gmail.com>
 */
class FeedController extends AbstractDefaultController
{
    /**
     * @return Response
     */
    public function index()
    {
//        dd($this->request);
        $manager = $this->em->getRepository('App\Entity\Feed');
        $page = (int)$this->request->query->get('page', 1);
        $this->nbItemsPerPage = (int)$this->request->query->get('itemPerPage', $this->nbItemsPerPage);
        $query = (string)$this->request->query->get('query', '');

        $entities = $manager->retrieve($this->nbItemsPerPage, $page, $query);
        return $this->json($entities, Response::HTTP_OK, [], ["groups" => "item:read"]);

//        $feeds = $feedRepository->findAll();
//        return $this->json($feeds, Response::HTTP_OK, [], ["groups" => "item:read"]);
    }

    /**
     * @return Response
     */
    public function countAll()
    {
        $manager = $this->em->getRepository('App\Entity\Feed');
        $count = $manager->countAll();
        return $this->json($count, Response::HTTP_OK);
    }

    /**
     * @param Feed $entity |null
     * @return RedirectResponse|Response
     *
     */
    public function find(Feed $entity = null)
    {
        if (!$entity)
            return $this->json([''], Response::HTTP_NOT_FOUND);
        return $this->json($entity, Response::HTTP_OK, [], ["groups" => "item:read"]);
    }

    /**
     * @param ValidatorInterface $validator
     * @return JsonResponse
     * @throws Exception
     *
     */
    public function add(Request $request, SerializerInterface $serializer, EntityManager $em, ValidatorInterface $validator)
    {
        $json = $request->getContent();
        $entity = $serializer->deserialize($json, Feed::class, 'json');
        $entity->setDateAdd(new \DateTime());

        $errors = $validator->validate($entity);
        if (!count($errors)) {
            try {
                $this->em->persist($entity);
                $this->em->flush();
                return $this->json($entity, Response::HTTP_OK, [], ["groups" => "item:read"]);
            } catch (Exception $e) {
                return $this->json($e, Response::HTTP_NOT_ACCEPTABLE);
            }
        }
        return $this->json($errors, Response::HTTP_BAD_REQUEST);
    }

}

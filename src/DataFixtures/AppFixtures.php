<?php

namespace App\DataFixtures;

use App\Entity\Feed;
use App\Entity\Opportunity;
use App\Entity\Recruiter;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $imageUrls = [
            "https://ambcrypto.com/wp-content/uploads/2022/11/CARNIVAL-1000x600.png",
            "https://static.seekingalpha.com/cdn/s3/uploads/getty_images/1325449542/image_1325449542.jpg?io=getty-c-w750",
            "https://media.wired.com/photos/6376d35240faa865b10965a7/191:100/w_1280,c_limit/elon-tweets-graphick6.jpg",
            "https://api.mkonsulting.com/uploads/avatar/logo-mk-wbc-png.png",
            "https://i.kinja-img.com/gawker-media/image/upload/c_fill,f_auto,fl_progressive,g_center,h_675,pg_1,q_80,w_1200/7977e00794fd35c92ef155787e748cb4.jpg",
            "https://nypost.com/wp-content/uploads/sites/2/2022/11/elon-musk-riptwitter-hashtag-comp.jpg?quality=75&strip=all&w=1024",
            "https://cdnn1.img.sputniknews.com/images/sharing/article/eng/1104405388.jpg?10831367431668779309",
            "https://api.mkonsulting.com/uploads/avatar/tbg1-png.png",
        ];
        $sectors = [
            "Etudes",
            "Diverse",
            "Conseil",
        ];

        $faker = Factory::create('fr_FR');

        for ($p = 0; $p < 50; $p++) {
            $feed = new Feed();
            $feed->setTitle($faker->catchPhrase)
                ->setContent($faker->text(255))
                ->setDescription($faker->text(255))
                ->setAuthor($faker->userName)
                ->setNumberOfLikes(mt_rand(1, 10))
                ->setNumberOfComments(mt_rand(1, 10))
                ->setImageUrl($imageUrls[mt_rand(0, 5)])
                ->setDateAdd($faker->dateTimeBetween('-6 months'));
            $manager->persist($feed);

            $opportunity = new Opportunity();
            $opportunity->setTitle($faker->jobTitle)
                ->setContent($faker->text(255))
                ->setDescription($faker->text(255))
                ->setAuthor($faker->userName)
                ->setNumberOfLikes(mt_rand(1, 10))
                ->setNumberOfComments(mt_rand(1, 10))
                ->setImageUrl($imageUrls[mt_rand(0, 7)])
                ->setDateAdd($faker->dateTimeBetween('-6 months'))
                ->setEndDate(new \DateTime('2022-12-25T15:00:00+02:00'))
                ->setReference($faker->text(10))
                ->setRequiredExperience(random_int(2, 10))
                ->setOpportunityType($faker->mimeType)
                ->setSectors($sectors);
            $manager->persist($opportunity);

            $recruiter = new Recruiter();
            $recruiter->setLastname($faker->lastName)
                ->setFirstname($faker->firstName)
                ->setEmail($faker->email)
                ->setNationality($faker->country)
                ->setLabel($faker->catchPhrase)
                ->setImageUrl($imageUrls[mt_rand(0, 7)]);
            $manager->persist($recruiter);

        }

        $manager->flush();
    }
}

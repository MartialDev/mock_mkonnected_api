<?php

namespace App\Entity;

use App\Repository\RecruiterRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=RecruiterRepository::class)
 */
class Recruiter extends AbstractDefault
{
    /**
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     * @Groups("item:read")
     * @Assert\NotBlank()
     */
    private $lastname;

    /**
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $firstname;

    /**
     * @ORM\Column(name="nationality", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $nationality;

    /**
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $email;

    /**
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $label;

    /**
     * @ORM\Column(name="image_url", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $imageUrl;

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     * @return Recruiter
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     * @return Recruiter
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     * @return Recruiter
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Recruiter
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     * @return Recruiter
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\OpportunityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PhpParser\Node\Expr\Array_;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OpportunityRepository::class)
 */
class Opportunity extends AbstractDefault
{
    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Groups("item:read")
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(name="opportunity_type", type="string", length=255, nullable=true)
     * @Groups("item:read")
     * @Assert\NotBlank()
     */
    private $opportunityType;
    /**
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $reference;

    /**
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $description;

    /**
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $author;

    /**
     * @ORM\Column(name="content", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $content;

    /**
     * @ORM\Column(name="sectors", type="array", nullable=true)
     * @Groups("item:read")
     */
    private $sectors;

    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     * @Groups("item:read")
     */
    private $endDate;

    /**
     * @ORM\Column(name="required_experience", type="integer", nullable=true)
     * @Groups("item:read")
     */
    private $requiredExperience;

    /**
     * @ORM\Column(name="number_of_likes", type="integer", nullable=true)
     * @Groups("item:read")
     */
    private $numberOfLikes;

    /**
     * @ORM\Column(name="number_of_comments", type="integer", nullable=true)
     * @Groups("item:read")
     */
    private $numberOfComments;

    /**
     * @ORM\Column(name="image_url", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $imageUrl;

    /**
     * Opportunity constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->sectors =  [];
    }


    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getNumberOfLikes(): ?int
    {
        return $this->numberOfLikes;
    }

    public function setNumberOfLikes(?int $numberOfLikes): self
    {
        $this->numberOfLikes = $numberOfLikes;

        return $this;
    }

    public function getNumberOfComments(): ?int
    {
        return $this->numberOfComments;
    }

    public function setNumberOfComments(?int $numberOfComments): self
    {
        $this->numberOfComments = $numberOfComments;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getOpportunityType()
    {
        return $this->opportunityType;
    }

    /**
     * @param string $opportunityType
     * @return Opportunity
     */
    public function setOpportunityType($opportunityType)
    {
        $this->opportunityType = $opportunityType;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return Opportunity
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getSectors()
    {
        return $this->sectors;
    }

    /**
     * @param string[] $sectors
     * @return Opportunity
     */
    public function setSectors($sectors)
    {
        $this->sectors = $sectors;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     * @return Opportunity
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getRequiredExperience()
    {
        return $this->requiredExperience;
    }

    /**
     * @param int $requiredExperience
     * @return Opportunity
     */
    public function setRequiredExperience($requiredExperience)
    {
        $this->requiredExperience = $requiredExperience;
        return $this;
    }
}

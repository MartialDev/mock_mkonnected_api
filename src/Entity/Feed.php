<?php

namespace App\Entity;

use App\Repository\FeedRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=FeedRepository::class)
 */
class Feed extends AbstractDefault
{
    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Groups("item:read")
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $description;

    /**
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $author;

    /**
     * @ORM\Column(name="content", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $content;

    /**
     * @ORM\Column(name="number_of_likes", type="integer", nullable=true)
     * @Groups("item:read")
     */
    private $numberOfLikes;

    /**
     * @ORM\Column(name="number_of_comments", type="integer", nullable=true)
     * @Groups("item:read")
     */
    private $numberOfComments;

    /**
     * @ORM\Column(name="image_url", type="string", length=255, nullable=true)
     * @Groups("item:read")
     */
    private $imageUrl;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getNumberOfLikes(): ?int
    {
        return $this->numberOfLikes;
    }

    public function setNumberOfLikes(?int $numberOfLikes): self
    {
        $this->numberOfLikes = $numberOfLikes;

        return $this;
    }

    public function getNumberOfComments(): ?int
    {
        return $this->numberOfComments;
    }

    public function setNumberOfComments(?int $numberOfComments): self
    {
        $this->numberOfComments = $numberOfComments;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }
}

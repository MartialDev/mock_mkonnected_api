<?php

namespace App\Repository;

use App\Entity\Opportunity;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Opportunity>
 *
 * @method Opportunity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Opportunity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Opportunity[]    findAll()
 * @method Opportunity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpportunityRepository extends AbstractDefaultRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Opportunity::class);
    }

    /**
     * Retrieve entities for pagination
     *
     * @param int $numberPerPage
     * @param int $page
     *
     * @param int $company_id
     * @param string $query
     * @param $fromDate
     * @param $toDate
     * @param array $args
     * @return Paginator
     * @throws \Exception
     */
    public function retrieve(int $numberPerPage, int $page, string $query, ...$args)
    {
        if ((int)$page < 1) {
            $page = 1;
        }
        if ((int)$numberPerPage < 1) {
            $numberPerPage = 20;
        }
        $qb = $this->createQueryBuilder('entity');
//        $qb->where('entity.deleted = 0');

        if (strlen(trim($query))) {
            $qb->andWhere($qb->expr()->like('entity.title', $qb->expr()->literal('%' . $query . '%')))
                ->orWhere($qb->expr()->like('entity.content', $qb->expr()->literal('%' . $query . '%')))
                ->orWhere($qb->expr()->like('entity.description', $qb->expr()->literal('%' . $query . '%')));
        }

        $qb->orderBy('entity.dateAdd', 'DESC');
        $query = $qb->getQuery();
        $query->setFirstResult(($page - 1) * $numberPerPage)
            ->setMaxResults($numberPerPage);

        return new Paginator($query);
    }

//    /**
//     * @return Opportunity[] Returns an array of Opportunity objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Opportunity
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

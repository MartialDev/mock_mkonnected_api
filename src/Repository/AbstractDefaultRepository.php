<?php

namespace App\Repository;

use App\Entity\AbstractDefault;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AbstractDefault>
 *
 * @method AbstractDefault|null find($id, $lockMode = null, $lockVersion = null)
 * @method AbstractDefault|null findOneBy(array $criteria, array $orderBy = null)
 * @method AbstractDefault[]    findAll()
 * @method AbstractDefault[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AbstractDefaultRepository extends ServiceEntityRepository
{
    /**
     * AbstractDefaultRepository constructor.
     * @param ManagerRegistry $registry
     * @param string $entityClass
     */
    public function __construct(ManagerRegistry $registry, $entityClass = AbstractDefault::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function add(AbstractDefault $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AbstractDefault $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Retrieve entities for pagination
     *
     * @param int $numberPerPage
     * @param int $page
     *
     * @param int $company_id
     * @param string $query
     * @param $fromDate
     * @param $toDate
     * @param array $args
     * @return Paginator
     * @throws \Exception
     */
    public function retrieve(int $numberPerPage, int $page, string $query, ...$args)
    {
        if ((int)$page < 1) {
            $page = 1;
        }
        if ((int)$numberPerPage < 1) {
            $numberPerPage = 20;
        }
        $qb = $this->createQueryBuilder('entity');
        $qb->where('entity.deleted = 0');

        $qb->orderBy('entity.dateAdd', 'DESC');

        $query = $qb->getQuery();
        $query->setFirstResult(($page - 1) * $numberPerPage)
            ->setMaxResults($numberPerPage);

        return new Paginator($query);
    }

    /**
     * @return int|mixed|string
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countAll()
    {
        $qb = $this->createQueryBuilder('entity');
        return $qb
            ->select('count(entity.id)')
//            ->where('entity.deleted = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

//    /**
//     * @return AbstractDefault[] Returns an array of AbstractDefault objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?AbstractDefault
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
